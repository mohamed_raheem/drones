package com.musala.drones.controller;

import com.musala.drones.common.DroneState;
import com.musala.drones.dto.DroneDto;
import com.musala.drones.dto.LoadDroneRequest;
import com.musala.drones.dto.MedicationDto;
import com.musala.drones.service.drone.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class DispatchController {

    private static final String BASE_PATH = "/api";
    public static final String DRONE_BASE_PATH = BASE_PATH + "/drone";
    public static final String MEDICATION_BASE_PATH = BASE_PATH + "/medication";
    public static final String LOAD_DRONE_PATH = DRONE_BASE_PATH + "/load";
    private final DroneService droneService;

    @Autowired
    public DispatchController(DroneService droneService) {
        this.droneService = droneService;
    }

    @PutMapping(path = DRONE_BASE_PATH, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DroneDto> registerDrone(@RequestBody DroneDto droneDto) {
        DroneDto newDrone = droneService.registerOrUpdateDrone(droneDto);
        return ResponseEntity.status(HttpStatus.OK).body(newDrone);
    }

    @GetMapping(path = DRONE_BASE_PATH + "/{droneSerialNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DroneDto> findDrone(@PathVariable String droneSerialNumber) {
        DroneDto drone = droneService.getDroneBySerialNumber(droneSerialNumber);
        return ResponseEntity.status(HttpStatus.OK).body(drone);
    }

    @PutMapping(path = LOAD_DRONE_PATH, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DroneDto> loadDrone(@RequestBody @Valid LoadDroneRequest loadDroneRequest) {
        DroneDto drone = droneService.loadDroneWithMedications(loadDroneRequest.getDroneSerialNumber(),
                loadDroneRequest.getMedications());
        return ResponseEntity.status(HttpStatus.OK).body(drone);
    }

    @GetMapping(path = MEDICATION_BASE_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MedicationDto>> getLoadedMedications(@RequestParam String droneSerialNumber) {
        List<MedicationDto> medications = droneService.findDroneMedications(droneSerialNumber);
        return ResponseEntity.status(HttpStatus.OK).body(medications);
    }

    @GetMapping(path = DRONE_BASE_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DroneDto>> getDroneByState(@RequestParam String state) {
        List<DroneDto> drones = droneService.getDroneByState(DroneState.valueOf(state));
        return ResponseEntity.status(HttpStatus.OK).body(drones);
    }
}
