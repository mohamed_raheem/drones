package com.musala.drones.service.medication;

import com.musala.drones.dto.MedicationDto;
import com.musala.drones.exception.BusinessException;
import com.musala.drones.model.Medication;
import com.musala.drones.repository.medication.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MedicationServiceImpl implements MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationServiceImpl(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Medication> findMedicationsByCode(Set<MedicationDto> medicationsDtoList) {
        List<String> codes = medicationsDtoList.stream().map(MedicationDto::getCode).collect(Collectors.toList());
        List<Medication> medications = medicationRepository.findByCodeIn(codes);
        if(medications.size() < codes.size()) {
            codes.removeAll(medications.stream().map(Medication::getCode).collect(Collectors.toList()));
            throw new BusinessException("ERR-02", "Medications: " + codes + " not found", null);
        }
        return medications;
    }
}
