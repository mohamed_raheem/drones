package com.musala.drones.service.medication;

import com.musala.drones.dto.MedicationDto;
import com.musala.drones.model.Medication;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public interface MedicationService {
    List<Medication> findMedicationsByCode(Set<MedicationDto> medicationsDtoList);

    static BigDecimal totalWeight(List<Medication> medications) {
        BigDecimal weight = BigDecimal.ZERO;
        for(Medication medication: medications) {
            weight = weight.add(medication.getWeight());
        }
        return weight;
    }
}
