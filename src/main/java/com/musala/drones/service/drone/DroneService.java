package com.musala.drones.service.drone;

import com.musala.drones.common.DroneState;
import com.musala.drones.dto.DroneDto;
import com.musala.drones.dto.MedicationDto;
import com.musala.drones.exception.BusinessException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public interface DroneService {
    DroneDto getDroneBySerialNumber(String droneSerialNumber);
    DroneDto registerOrUpdateDrone(DroneDto droneDto);
    DroneDto loadDroneWithMedications(String droneSerialNumber, Set<MedicationDto> medicationCode);
    List<MedicationDto> findDroneMedications(String droneSerialNumber);
    List<DroneDto> getDroneByState(DroneState droneState);

    static void requiresIdleState(DroneState currentState) {
        if(!currentState.equals(DroneState.IDLE)) {
            throw new BusinessException("ERR-04", "Drone is busy: " + currentState.name(), null);
        }
    }

    static void requiresMinBatteryCapacity25(int currentBatteryCapacity) {
        if(currentBatteryCapacity < 25) {
            throw new BusinessException("ERR-05", "Drone battery capacity less than " + 25 + " %", null);
        }
    }

    static void requiresLessMedicationsWeight(BigDecimal droneWeight, BigDecimal medicationsWeight) {
        if(medicationsWeight.compareTo(droneWeight) > 0) {
            throw new BusinessException("ERR-03", "Medications weight: " + medicationsWeight +
                    "gm exceed drone capacity: " + droneWeight + "gm", null);
        }
    }
}
