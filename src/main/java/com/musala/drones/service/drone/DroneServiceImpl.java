package com.musala.drones.service.drone;

import com.musala.drones.common.DroneState;
import com.musala.drones.dto.DroneDto;
import com.musala.drones.dto.MedicationDto;
import com.musala.drones.exception.BusinessException;
import com.musala.drones.model.Drone;
import com.musala.drones.model.Medication;
import com.musala.drones.repository.drone.DroneRepository;
import com.musala.drones.service.medication.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;
    private final MedicationService medicationService;

    @Autowired
    public DroneServiceImpl(DroneRepository droneRepository, MedicationService medicationService) {
        this.droneRepository = droneRepository;
        this.medicationService = medicationService;
    }

    @Override
    @Transactional
    public DroneDto registerOrUpdateDrone(DroneDto droneDto) {

        Drone drone;
        Optional<Drone> existingDrone = droneRepository.findBySerialNumber(droneDto.getSerialNumber());
        if(existingDrone.isPresent()) {
            // merge and update
            drone = existingDrone.get();
            drone.setBatteryCapacity(droneDto.getBatteryCapacity());
            drone.setWeight(droneDto.getWeight());
            drone.setState(droneDto.getState());
            drone.setModel(droneDto.getModel());
        } else {
            drone = Drone.fromDroneDto(droneDto);
        }

        drone = droneRepository.saveAndFlush(drone);
        return DroneDto.fromDrone(drone);
    }

    @Override
    @Transactional
    public DroneDto loadDroneWithMedications(String droneSerialNumber, Set<MedicationDto> medicationDtoList) {

        Drone drone = findDroneBySerialNumber(droneSerialNumber);
        DroneService.requiresIdleState(drone.getState());
        DroneService.requiresMinBatteryCapacity25(drone.getBatteryCapacity());

        List<Medication> medications = medicationService.findMedicationsByCode(medicationDtoList);

        BigDecimal medicationsTotalWeight = MedicationService.totalWeight(medications);
        DroneService.requiresLessMedicationsWeight(drone.getWeight(), medicationsTotalWeight);

        drone.reloadMedications(medications);
        drone.setState(DroneState.LOADING);

        return DroneDto.fromDrone(drone);
    }

    @Override
    public DroneDto getDroneBySerialNumber(String droneSerialNumber) {
        return DroneDto.fromDrone(findDroneBySerialNumber(droneSerialNumber));
    }

    @Transactional(readOnly = true)
    private Drone findDroneBySerialNumber(String droneSerialNumber) {
        Optional<Drone> drone = droneRepository.findBySerialNumber(droneSerialNumber);
        if(drone.isEmpty()) {
            throw new BusinessException("ERR-01", "No drone with serial: " + droneSerialNumber + " exists", null);
        }

        return drone.get();
    }

    @Override
    public List<MedicationDto> findDroneMedications(String droneSerialNumber) {
        Drone drone = findDroneBySerialNumber(droneSerialNumber);
        return MedicationDto.fromMedicationList(drone.getMedications());
    }

    @Override
    public List<DroneDto> getDroneByState(DroneState droneState) {
        List<Drone> drones = droneRepository.findByState(droneState);
        return DroneDto.fromDronesList(drones);
    }
}
