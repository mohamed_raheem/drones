package com.musala.drones.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.Objects;

@Entity(name = "DroneBatteryState")
@Table(name = "DRONE_BATTERY_STATE")
@Getter @Setter
public class DroneBatteryState {

    DroneBatteryState() { }

    public DroneBatteryState(String serialNumber, int batteryCapacity, Date timeStamp) {
        this.serialNumber = serialNumber;
        this.batteryCapacity = batteryCapacity;
        this.timeStamp = timeStamp;
    }

    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Setter(AccessLevel.NONE)
    @NotBlank(message = "drone serial number can not be null or empty")
    @Size(max = 100, message = "drone serial number must be less that 100 characters")
    @Column(name = "SERIAL_NUMBER", nullable = false, length = 100)
    private String serialNumber;

    @Column(name = "BATTERY_CAPACITY", nullable = false)
    @Min(value = 0, message = "drone battery capacity must be positive")
    @Max(value = 100, message = "drone battery capacity must not exceed 100%")
    private int batteryCapacity;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TIMESTAMP", nullable = false)
    private Date timeStamp;

    @Version
    private int version;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DroneBatteryState that = (DroneBatteryState) o;
        return Objects.equals(serialNumber, that.serialNumber) && Objects.equals(timeStamp, that.timeStamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serialNumber, timeStamp);
    }
}
