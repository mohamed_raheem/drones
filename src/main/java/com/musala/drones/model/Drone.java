package com.musala.drones.model;

import com.musala.drones.common.DroneModel;
import com.musala.drones.common.DroneState;
import com.musala.drones.dto.DroneDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "DRONE")
@Getter @Setter @ToString
public class Drone {

    Drone() {}

    public Drone(String serialNumber) {
      this.serialNumber = serialNumber;
    }

    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Setter(AccessLevel.NONE)
    @NotBlank(message = "drone serial number can not be null or empty")
    @Size(max = 100, message = "drone serial number must be less that 100 characters")
    @NaturalId
    @Column(name = "SERIAL_NUMBER", nullable = false, unique = true, length = 100)
    private String serialNumber;

    @Enumerated(value = EnumType.ORDINAL)
    @Column(name = "MODEL")
    private DroneModel model;

    @Column(name = "WEIGHT", nullable = false)
    @Min(value = 0, message = "drone weight must be positive")
    @Max(value = 500, message = "drone weight must not exceed 500 grams")
    private BigDecimal weight;

    @Column(name = "BATTERY_CAPACITY", nullable = false)
    @Min(value = 0, message = "drone battery capacity must be positive")
    @Max(value = 100, message = "drone battery capacity must not exceed 100%")
    private int batteryCapacity;

    @Enumerated(value = EnumType.ORDINAL)
    @Column(name = "STATE", nullable = false)
    private DroneState state;

    @ToString.Exclude
    @OneToMany(mappedBy = "drone", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Medication> medications = new ArrayList<>();

    @Version
    private int version;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Drone drone = (Drone) o;
        return Objects.equals(serialNumber, drone.serialNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serialNumber);
    }

    public static Drone fromDroneDto(DroneDto droneDto) {
        Drone drone = new Drone(droneDto.getSerialNumber());
        drone.setBatteryCapacity(droneDto.getBatteryCapacity());
        drone.setWeight(droneDto.getWeight());
        drone.setModel(droneDto.getModel());
        drone.setState(droneDto.getState());
        return drone;
    }

    public void reloadMedications(List<Medication> newMedications) {

        Iterator<Medication> iterator = this.medications.iterator();
        while(iterator.hasNext()) {
            iterator.next().setDrone(null);
            iterator.remove();
        }

        for(Medication medication: newMedications) {
            loadMedication(medication);
        }
    }

    public void loadMedication(Medication medication) {
        this.medications.add(medication);
        medication.setDrone(this);
    }

    public void unloadMedication(Medication medication) {
        this.medications.remove(medication);
        medication.setDrone(null);
    }
}
