package com.musala.drones.model;

import com.musala.drones.dto.MedicationDto;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "MEDICATION")
@Getter @Setter @ToString
public class Medication {

    Medication() { }

    public Medication(String code) {
        this.code = code;
    }

    @Setter(AccessLevel.NONE)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Setter(AccessLevel.NONE)
    @NotNull(message = "medication code can not be null")
    @Pattern(regexp = "^[A-Z0-9_]+$", message = "Invalid medication code")
    @Column(name = "CODE", nullable = false, unique = true)
    @NaturalId
    private String code;

    @NotNull(message = "medication name can not be null")
    @Pattern(regexp = "^[a-zA-Z0-9-_]+$", message = "Invalid medication name")
    @Column(name = "NAME", nullable = false, unique = true)
    private String name;

    @Column(name = "WEIGHT", nullable = false)
    private BigDecimal weight;

    @Lob
    @Column(name = "IMAGE")
    private byte[] image;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DRONE_ID")
    private Drone drone;

    @Version
    private int version;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medication that = (Medication) o;
        return Objects.equals(code, that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }


    public static Medication fromMedicationDto(MedicationDto medicationDto) {
        Medication medication = new Medication(medicationDto.getCode());
        medication.setName(medicationDto.getName());
        medication.setWeight(medicationDto.getWeight());
        medication.setImage(medicationDto.getImage());
        return medication;
    }
}
