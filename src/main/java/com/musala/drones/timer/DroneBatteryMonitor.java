package com.musala.drones.timer;

import com.musala.drones.model.Drone;
import com.musala.drones.model.DroneBatteryState;
import com.musala.drones.repository.drone.DroneBatteryRepository;
import com.musala.drones.repository.drone.DroneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class DroneBatteryMonitor {

    private final Logger LOGGER = LoggerFactory.getLogger(DroneBatteryMonitor.class);
    private final DroneRepository droneRepository;
    private final DroneBatteryRepository droneBatteryRepository;

    @Autowired
    public DroneBatteryMonitor(DroneRepository droneRepository, DroneBatteryRepository droneBatteryRepository) {
        this.droneRepository = droneRepository;
        this.droneBatteryRepository = droneBatteryRepository;
    }

    @Transactional
    @Scheduled(fixedDelay = 5000)
    public void checkDronesBattery() {
        LOGGER.info("scheduler fired");
        List<Drone> drones = droneRepository.findAll();

        for(Drone drone: drones) {
            DroneBatteryState droneBatteryState = new DroneBatteryState(
                    drone.getSerialNumber(), drone.getBatteryCapacity(), new Date());
            droneBatteryRepository.save(droneBatteryState);
        }
    }
}
