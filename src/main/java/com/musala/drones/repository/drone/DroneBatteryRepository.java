package com.musala.drones.repository.drone;

import com.musala.drones.model.DroneBatteryState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DroneBatteryRepository extends JpaRepository<DroneBatteryState, Long> {
}
