package com.musala.drones.exception.handler;

import com.musala.drones.dto.ErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

@ControllerAdvice
public class GeneralExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleGenericError(Exception ex) {
        LOGGER.error("Generic exception", ex);
        ErrorResponse error = new ErrorResponse("SRVR-000", "Something wrong happened");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @ExceptionHandler(DataAccessException.class)
    public ResponseEntity<ErrorResponse> handleGenericError(DataAccessException ex) {
        LOGGER.error("Data access exception", ex);
        ErrorResponse error = new ErrorResponse("SRVR-001", "Database problem");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> handleValidationError(ConstraintViolationException ex) {
        LOGGER.error("Validation exception", ex);
        ErrorResponse error = new ErrorResponse("VAL-000", resolveConstraintViolationsErrorMessage(ex.getConstraintViolations()));
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(error);
    }

    private static String resolveConstraintViolationsErrorMessage(Set<ConstraintViolation<?>> violations) {
        StringBuilder message = new StringBuilder();
        for(ConstraintViolation<?> constraintViolation: violations) {
            message.append(constraintViolation.getMessage()).append(",");
        }
        return message.toString();
    }

}
