package com.musala.drones.exception;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BusinessException extends RuntimeException {
    private String errorCode;
    private String errorMessage;

    public BusinessException(String errorCode, String errorMessage, Exception ex) {
        super(ex);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }
}
