package com.musala.drones.dto;

import com.musala.drones.model.Medication;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter @Setter
public class MedicationDto {

    private String code;
    private String name;
    private BigDecimal weight;
    private byte[] image;

    public static MedicationDto fromMedication(Medication medication) {
        MedicationDto medicationDto = new MedicationDto();
        medicationDto.setCode(medication.getCode());
        medicationDto.setName(medication.getName());
        medicationDto.setWeight(medication.getWeight());
        medicationDto.setImage(medication.getImage());
        return medicationDto;
    }

    public static List<MedicationDto> fromMedicationList(List<Medication> medicationList) {
        List<MedicationDto> medications = new ArrayList<>();
        for(Medication medication: medicationList) {
            medications.add(MedicationDto.fromMedication(medication));
        }
        return medications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDto that = (MedicationDto) o;
        return Objects.equals(code, that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
