package com.musala.drones.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Getter @Setter
public class LoadDroneRequest {

    @NotEmpty
    private Set<MedicationDto> medications;
    @NotBlank
    private String droneSerialNumber;
}
