package com.musala.drones.dto;

import com.musala.drones.common.DroneModel;
import com.musala.drones.common.DroneState;
import com.musala.drones.model.Drone;
import com.musala.drones.model.Medication;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Getter @Setter @ToString
public class DroneDto {

    private String serialNumber;
    private DroneModel model;
    private BigDecimal weight;
    private int batteryCapacity;
    private DroneState state;
    private List<MedicationDto> medications = new ArrayList<>();

    DroneDto() {}

    public DroneDto(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public static DroneDto fromDrone(Drone drone) {
        DroneDto droneDto = new DroneDto(drone.getSerialNumber());
        droneDto.setBatteryCapacity(drone.getBatteryCapacity());
        droneDto.setWeight(drone.getWeight());
        droneDto.setModel(drone.getModel());
        droneDto.setState(drone.getState());
        for (Medication medication: drone.getMedications()) {
            droneDto.getMedications().add(MedicationDto.fromMedication(medication));
        }
        return droneDto;
    }

    public static List<DroneDto> fromDronesList(List<Drone> drones) {
        return drones.stream().map(DroneDto::fromDrone).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DroneDto droneDto = (DroneDto) o;
        return Objects.equals(serialNumber, droneDto.serialNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(serialNumber);
    }
}
