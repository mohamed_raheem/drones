package com.musala.drones.service.drone;

import com.musala.drones.common.DroneModel;
import com.musala.drones.common.DroneState;
import com.musala.drones.dto.DroneDto;
import com.musala.drones.repository.drone.DroneRepository;
import com.musala.drones.repository.medication.MedicationRepository;
import com.musala.drones.service.medication.MedicationService;
import com.musala.drones.service.medication.MedicationServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Map;

@DataJpaTest
@ExtendWith(SpringExtension.class)
@Import(DroneServiceIntegrationTest.Config.class)
public class DroneServiceIntegrationTest {

    @Autowired
    private DroneService droneService;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    @Transactional
    public void givenNoDroneWithSameSerialExists_whenRegisterDrone_thenCreateNew() {

        String serialNum = "123-456";
        DroneDto droneDto = new DroneDto(serialNum);
        droneDto.setModel(DroneModel.CRUISER_WEIGHT);
        droneDto.setBatteryCapacity(55);
        droneDto.setState(DroneState.DELIVERING);
        droneDto.setWeight(new BigDecimal("105.25"));

        // verify response
        DroneDto actualDroneDto = droneService.registerOrUpdateDrone(droneDto);

        Assertions.assertEquals(droneDto.getSerialNumber(), actualDroneDto.getSerialNumber());
        Assertions.assertEquals(droneDto.getModel(), actualDroneDto.getModel());
        Assertions.assertEquals(droneDto.getBatteryCapacity(), actualDroneDto.getBatteryCapacity());
        Assertions.assertEquals(droneDto.getState(), actualDroneDto.getState());
        Assertions.assertEquals(droneDto.getWeight(), actualDroneDto.getWeight());

        // verify DB state
        Map<String, Object> resultSet =
                jdbcTemplate.queryForMap("SELECT * FROM DRONE WHERE SERIAL_NUMBER='" + serialNum + "'");
        Assertions.assertNotNull(resultSet.get("ID"));
        Assertions.assertEquals(serialNum, resultSet.get("SERIAL_NUMBER"));
        Assertions.assertEquals(DroneModel.CRUISER_WEIGHT.ordinal(), resultSet.get("MODEL"));
        Assertions.assertEquals(55, resultSet.get("BATTERY_CAPACITY"));
        Assertions.assertEquals(DroneState.DELIVERING.ordinal(), resultSet.get("STATE"));
        Assertions.assertEquals(new BigDecimal("105.25"), resultSet.get("WEIGHT"));
    }

    @Test
    @Sql(statements = {"INSERT INTO DRONE(ID, SERIAL_NUMBER, MODEL, BATTERY_CAPACITY, STATE, WEIGHT, VERSION) values " +
            "(1000, '123-456', 0, 20, 2, 250.22, 1);"})
    public void givenDroneWithSameSerialExists_whenRegisterDrone_thenUpdate() {

        String serialNum = "123-456";
        DroneDto droneDto = new DroneDto(serialNum);
        droneDto.setModel(DroneModel.CRUISER_WEIGHT);
        droneDto.setBatteryCapacity(55);
        droneDto.setState(DroneState.DELIVERING);
        droneDto.setWeight(new BigDecimal("105.25"));

        // verify response
        DroneDto actualDroneDto = droneService.registerOrUpdateDrone(droneDto);
        Assertions.assertEquals(droneDto.getSerialNumber(), actualDroneDto.getSerialNumber());
        Assertions.assertEquals(droneDto.getModel(), actualDroneDto.getModel());
        Assertions.assertEquals(droneDto.getBatteryCapacity(), actualDroneDto.getBatteryCapacity());
        Assertions.assertEquals(droneDto.getState(), actualDroneDto.getState());
        Assertions.assertEquals(droneDto.getWeight(), actualDroneDto.getWeight());

        // verify DB state
        Map<String, Object> resultSet =
                jdbcTemplate.queryForMap("SELECT * FROM DRONE WHERE SERIAL_NUMBER='" + serialNum + "'");
        Assertions.assertEquals(1000L, resultSet.get("ID"));
        Assertions.assertEquals(serialNum, resultSet.get("SERIAL_NUMBER"));
        Assertions.assertEquals(DroneModel.CRUISER_WEIGHT.ordinal(), resultSet.get("MODEL"));
        Assertions.assertEquals(55, resultSet.get("BATTERY_CAPACITY"));
        Assertions.assertEquals(DroneState.DELIVERING.ordinal(), resultSet.get("STATE"));
        Assertions.assertEquals(new BigDecimal("105.25"), resultSet.get("WEIGHT"));
    }

    public static class Config {

        @Autowired
        private DroneRepository droneRepository;

        @Autowired
        private MedicationRepository medicationRepository;

        @Bean
        public DroneService droneService() {
            return new DroneServiceImpl(droneRepository, medicationService());
        }

        @Bean
        public MedicationService medicationService() {
            return new MedicationServiceImpl(medicationRepository);
        }
    }
}