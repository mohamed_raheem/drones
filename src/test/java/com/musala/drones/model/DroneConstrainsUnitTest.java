package com.musala.drones.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DroneConstrainsUnitTest {

    private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    @Nested
    public class SerialNumberValidations {

        @Test
        public void givenNullSerialNumber_whenValidateDrone_thenThrowIllegalArgumentException() {
            Drone drone = new Drone(null);
            List<ConstraintViolation<Drone>> constraintViolationSet =
                    new ArrayList<>(VALIDATOR.validateProperty(drone, "serialNumber"));

            Assertions.assertFalse(constraintViolationSet.isEmpty());
            Assertions.assertEquals(1, constraintViolationSet.size());
            Assertions.assertEquals("drone serial number can not be null or empty",
                    constraintViolationSet.get(0).getMessage());
        }

        @Test
        public void givenEmptySerialNumber_whenValidateDrone_thenThrowIllegalArgumentException() {
            Drone drone = new Drone(" ");
            List<ConstraintViolation<Drone>> constraintViolationSet =
                    new ArrayList<>(VALIDATOR.validateProperty(drone, "serialNumber"));

            Assertions.assertFalse(constraintViolationSet.isEmpty());
            Assertions.assertEquals(1, constraintViolationSet.size());
            Assertions.assertEquals("drone serial number can not be null or empty",
                    constraintViolationSet.get(0).getMessage());
        }

        @Test
        public void givenSerialNumberLongerThan100Chars_whenValidateDrone_thenConstrainViolation() {
            Drone drone= new Drone("a".repeat(200));
            List<ConstraintViolation<Drone>> constraintViolationSet =
                    new ArrayList<>(VALIDATOR.validateProperty(drone, "serialNumber"));

            Assertions.assertFalse(constraintViolationSet.isEmpty());
            Assertions.assertEquals(1, constraintViolationSet.size());
            Assertions.assertEquals("drone serial number must be less that 100 characters",
                    constraintViolationSet.get(0).getMessage());
        }

        @Test
        public void givenSerialNumberNotBlankAndLessThan100Chars_whenValidateDrone_thenSuccess() {
            Drone drone= new Drone("a".repeat(70));
            List<ConstraintViolation<Drone>> constraintViolationSet =
                    new ArrayList<>(VALIDATOR.validateProperty(drone, "serialNumber"));

            Assertions.assertTrue(constraintViolationSet.isEmpty());
        }
    }
    
    @Nested
    public class WeightValidations {

        @Test
        public void givenNegativeWeight_whenValidateDrone_thenConstrainViolation() {
            Drone drone = new Drone("drone");
            drone.setWeight(new BigDecimal("-10"));

            List<ConstraintViolation<Drone>> constraintViolationSet =
                    new ArrayList<>(VALIDATOR.validateProperty(drone, "weight"));

            Assertions.assertFalse(constraintViolationSet.isEmpty());
            Assertions.assertEquals(1, constraintViolationSet.size());
            Assertions.assertEquals("drone weight must be positive",
                    constraintViolationSet.get(0).getMessage());
        }

        @Test
        public void givenWeightMoreThan500Grams_whenValidateDrone_thenConstrainViolation() {
            Drone drone = new Drone("drone");
            drone.setWeight(new BigDecimal("500.01"));

            List<ConstraintViolation<Drone>> constraintViolationSet =
                    new ArrayList<>(VALIDATOR.validateProperty(drone, "weight"));

            Assertions.assertFalse(constraintViolationSet.isEmpty());
            Assertions.assertEquals(1, constraintViolationSet.size());
            Assertions.assertEquals("drone weight must not exceed 500 grams",
                    constraintViolationSet.get(0).getMessage());
        }

        @Test
        public void givenPositiveWeightLessThanEqual500_whenValidateDrone_thenSuccess() {
            Drone drone = new Drone("drone");
            drone.setWeight(new BigDecimal("500.00"));

            List<ConstraintViolation<Drone>> constraintViolationSet =
                    new ArrayList<>(VALIDATOR.validateProperty(drone, "weight"));

            Assertions.assertTrue(constraintViolationSet.isEmpty());

        }
    }

    @Nested
    public class BatteryCapacityValidations {

        @Test
        public void givenNegativeCapacity_whenValidateDrone_thenConstrainViolation() {
            Drone drone = new Drone("drone");
            drone.setBatteryCapacity(-20);

            List<ConstraintViolation<Drone>> constraintViolationSet =
                    new ArrayList<>(VALIDATOR.validateProperty(drone, "batteryCapacity"));

            Assertions.assertFalse(constraintViolationSet.isEmpty());
            Assertions.assertEquals(1, constraintViolationSet.size());
            Assertions.assertEquals("drone battery capacity must be positive",
                    constraintViolationSet.get(0).getMessage());
        }

        @Test
        public void givenCapacityMoreThan100_whenValidateDrone_thenConstrainViolation() {
            Drone drone = new Drone("drone");
            drone.setBatteryCapacity(105);

            List<ConstraintViolation<Drone>> constraintViolationSet =
                    new ArrayList<>(VALIDATOR.validateProperty(drone, "batteryCapacity"));

            Assertions.assertFalse(constraintViolationSet.isEmpty());
            Assertions.assertEquals(1, constraintViolationSet.size());
            Assertions.assertEquals("drone battery capacity must not exceed 100%",
                    constraintViolationSet.get(0).getMessage());
        }
    }

}
