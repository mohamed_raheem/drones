package com.musala.drones.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.drones.common.DroneModel;
import com.musala.drones.common.DroneState;
import com.musala.drones.dto.DroneDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class DispatchControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void givenDroneWithSerialNumberNotExist_whenRegister_thenCreate() throws Exception {

        DroneDto droneDto = new DroneDto("123-456");
        droneDto.setModel(DroneModel.CRUISER_WEIGHT);
        droneDto.setBatteryCapacity(55);
        droneDto.setState(DroneState.DELIVERING);
        droneDto.setWeight(new BigDecimal("105.25"));

        this.mockMvc.perform(put(DispatchController.DRONE_BASE_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(droneDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.serialNumber", is(equalTo(droneDto.getSerialNumber()))))
                .andExpect(jsonPath("$.model", is(equalTo(droneDto.getModel().name()))))
                .andExpect(jsonPath("$.batteryCapacity", is(equalTo(droneDto.getBatteryCapacity()))))
                .andExpect(jsonPath("$.state", is(equalTo(droneDto.getState().name()))))
                .andExpect(jsonPath("$.weight", is(equalTo(droneDto.getWeight().doubleValue()))));
    }

    @Test
    @Sql(statements = "insert into DRONE(ID, SERIAL_NUMBER, MODEL, BATTERY_CAPACITY, STATE, WEIGHT, VERSION) values " +
            "(1, '123-456', 2, 60, 0, 100, 1)")
    public void givenDroneWithSerialNumberExist_whenRegister_thenUpdate() throws Exception {

        DroneDto updatedDroneDto = new DroneDto("123-456");
        updatedDroneDto.setModel(DroneModel.CRUISER_WEIGHT);
        updatedDroneDto.setBatteryCapacity(55);
        updatedDroneDto.setState(DroneState.DELIVERING);
        updatedDroneDto.setWeight(new BigDecimal("105.25"));

        this.mockMvc.perform(put(DispatchController.DRONE_BASE_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedDroneDto)))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.serialNumber", is(equalTo(updatedDroneDto.getSerialNumber()))))
                .andExpect(jsonPath("$.model", is(equalTo(updatedDroneDto.getModel().name()))))
                .andExpect(jsonPath("$.batteryCapacity", is(equalTo(updatedDroneDto.getBatteryCapacity()))))
                .andExpect(jsonPath("$.state", is(equalTo(updatedDroneDto.getState().name()))))
                .andExpect(jsonPath("$.weight", is(equalTo(updatedDroneDto.getWeight().doubleValue()))));
    }
}
