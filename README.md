# Project Description #
We have a fleet of 10 drones. A drone is capable of carrying devices, other
than cameras, and capable of delivering small loads. For our use case the load is medications.

#### A Drone has ####
* serial number (100 characters max)
* model (Lightweight, Middleweight, Cruiserweight, Heavyweight)
* weight limit (500gr max)
* battery capacity (percentage)
* state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING)

#### Each Medication has ####
* name (allowed only letters, numbers, ‘-‘, ‘_’)
* weight
* code (allowed only upper case letters, underscore and numbers)
* image (picture of the medication case)

Develop a service via REST API that allows clients to communicate with the drones (i.e. dispatch controller)

#### The service should allow ####
* registering a drone
* loading a drone with medication items
* checking loaded medication items for a given drone
* checking available drones for loading;
* check drone battery level for a given drone
* Prevent the drone from being loaded with more weight that it can carry
* Prevent the drone from being in LOADING state if the battery level is below 25%
* Introduce a periodic task to check drones battery levels and create history/audit event log for this

---

## Tools ##
* Spring boot 2.6.0 with H2 DB
* Java 11
* maven 3.6.3

## Run Instructions ##
* Run the tests using:
````text
mvn clean test
````
* Run the application using:
````text
mvn spring-boot run
````

---

## APIs ##
Each API returns a different response as described next. However, 
all return the following structure in case of errors:
````json
{
    "errorCode": "VAL-000",
    "errorMessage": "drone weight must not exceed 500 grams,"
}
````
#### Register drone: ####
* url: PUT localhost:8080/api/drone (create/update a drone)
* Headers: {Content-Type: application/json}
* Request:
````json
  {
	"serialNumber" : "SER-12",
	"batteryCapacity":"98",
	"model":"HEAVY_WEIGHT",
	"state":"IDLE",
	"weight":"15.33"
}
````
* Response: (HTTP 200)
````json
  {
    "serialNumber": "SER-12",
    "model": "HEAVY_WEIGHT",
    "weight": 15.33,
    "batteryCapacity": 98,
    "state": "IDLE",
    "medications": []
}
````

#### Load drone: ####
* url: PUT localhost:8080/api/drone/load
* Headers: {Content-Type: application/json}
* Request:
````json
  {
	"medications" :[
		{"code": "MED2"}
	],
	"droneSerialNumber":"SER-1"
}
````
* Response: (HTTP 200)
````json
  {
    "serialNumber": "SER-1",
    "model": "MIDDLE_WEIGHT",
    "weight": 500,
    "batteryCapacity": 40,
    "state": "LOADING",
    "medications": [
        {
            "code": "MED2",
            "name": "MedicationTwo",
            "weight": 15,
            "image": null
        }
    ]
}
````

#### Get loaded medications for drone: ####
* url: GET localhost:8080/api/medication?droneSerialNumber=SER-1
* Response: (HTTP 200)
````json
  [
    {
      "code": "MED2",
      "name": "MedicationTwo",
      "weight": 15,
      "image": null
    }
  ]
````

#### Check available drones for loading: ####
* url: GET localhost:8080/api/drone?state=IDLE
* Response: (HTTP 200)
````json
  [
    {
      "serialNumber": "SER-3",
      "model": "LIGHT_WEIGHT",
      "weight": 400,
      "batteryCapacity": 55,
      "state": "IDLE",
      "medications": []
    }
  ]
````

#### Check battery status for drone: ####
* url: GET localhost:8080/api/drone/SER-1
* Response: (HTTP 200)
````json
  {
      "serialNumber": "SER-1",
      "model": "MIDDLE_WEIGHT",
      "weight": 500,
      "batteryCapacity": 40,
      "state": "LOADING",
      "medications": [
        {
          "code": "MED2",
          "name": "MedicationTwo",
          "weight": 15,
          "image": null
        }
      ]
  }
````

---

## Preloaded Dataset: ##

### Drones ###
| ID    | Serial | Model | Weight  | Battery capacity | State |
| :---: | :----: | :---: | :----:  | :--------------: | :---: |
| 1     | SER-1  | MIDDLE_WEIGHT   | 500gm    | 40%   | IDLE     |
| 2     | SER-2  | CRUISER_WEIGHT  | 470gm    | 90%   | DELIVERED     |
| 3     | SER-3  | LIGHT_WEIGHT    | 400gm    | 10%   | IDLE     |
| 4     | SER-4  | HEAVY_WEIGHT    | 390.20gm | 100%  | DELIVERED     |

### Medications ###
| ID    | Code   | Name            | Weight | Drone Id |
| :---: | :----: | :-------------: | :----: | :---:    |
| 1     | MED1   | MedicationOne   | 35gm   | 1        |
| 2     | MED2   | MedicationTwo   | 15gm   | 1        |
| 3     | MED3   | MedicationThree | 490gm  | 2        |